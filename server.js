const pup = require('puppeteer');

async function monitor() {
  const browser = await pup.launch({headless: true});
  const tab = await browser.newPage();
  await tab.setRequestInterception(true);
  tab.on('request', interceptedRequest => {
    console.log(new Date().toISOString().split('T')+interceptedRequest.url());
    interceptedRequest.continue();
  });
  var sTime = new Date();
  await tab.goto('https://google.com');
  console.log("Total load time : "+(new Date() - sTime)/1000+" Sec"); 
//  await tab.setViewport({width: 1920, height: 1080});
//  await tab.screenshot({path: 'google_'+Number(new Date())+'.jpg'});

  await browser.close();
}

monitor(); 